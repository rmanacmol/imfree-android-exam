package com.mobiledev.rpm.imfreeandroidexam.network.model;

/**
 * Created by rmanacmol on 3/31/2017.
 */

public class Mlocation {

    private double longitude;
    private String title;
    private double latitude;

    public Mlocation(String title, double latitude, double longitude) {
        this.longitude = longitude;
        this.title = title;
        this.latitude = latitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}

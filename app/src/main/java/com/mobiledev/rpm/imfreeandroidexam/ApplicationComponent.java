package com.mobiledev.rpm.imfreeandroidexam;

import com.mobiledev.rpm.imfreeandroidexam.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by rmanacmol on 3/30/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(ApplicationImFreeAndroidExam application);

    void inject(MainActivity activity);

}
package com.mobiledev.rpm.imfreeandroidexam;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by rmanacmol on 3/30/2017.
 */

public class ApplicationImFreeAndroidExam extends Application {

    private static ApplicationImFreeAndroidExam instance;
    protected ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null) {
            instance = this;
            mApplicationComponent =  DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        mApplicationComponent.inject(this);

        Timber.plant(new Timber.DebugTree() {
            @Override
            protected String createStackElementTag(StackTraceElement element) {
                return super.createStackElementTag(element) + ":" + element.getLineNumber();
            }
        });
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}

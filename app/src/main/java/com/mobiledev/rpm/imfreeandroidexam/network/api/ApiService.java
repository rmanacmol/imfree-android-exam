package com.mobiledev.rpm.imfreeandroidexam.network.api;

import com.mobiledev.rpm.imfreeandroidexam.network.model.Store;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by rmanacmol on 3/30/2017.
 */

public interface ApiService {

    @GET("jsonblob/92a45489-151c-11e7-a0ba-29c762df46e8")
    Observable<Store> getStore();

}

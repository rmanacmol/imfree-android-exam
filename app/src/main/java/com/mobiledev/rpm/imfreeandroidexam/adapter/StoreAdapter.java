package com.mobiledev.rpm.imfreeandroidexam.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobiledev.rpm.imfreeandroidexam.Constant;
import com.mobiledev.rpm.imfreeandroidexam.R;
import com.mobiledev.rpm.imfreeandroidexam.activity.StoreDetailsActivity;
import com.mobiledev.rpm.imfreeandroidexam.network.model.StoreData;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rmanacmol on 3/30/2017.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {

    private List<StoreData> mStore;
    private Context mContext;

    public StoreAdapter(List<StoreData> mStore, Context context) {
        this.mStore = mStore;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_store, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final StoreData storeData = mStore.get(position);

        holder.tvStoreName.setText(storeData.getName());
        holder.tvStoreDesc.setText(storeData.getDetails());
        Glide.with(mContext)
                .load(mStore.get(position).getImg_thumb())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivStoreImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, StoreDetailsActivity.class);
                i.putExtra(Constant.storeName, storeData.getName());
                i.putExtra(Constant.storeDesc, storeData.getDetails());
                i.putExtra(Constant.storeImage, storeData.getImg_header());
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvStoreName) TextView tvStoreName;
        @Bind(R.id.tvStoreDesc) TextView tvStoreDesc;
        @Bind(R.id.ivStoreImage) ImageView ivStoreImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setClickable(true);
        }
    }
}

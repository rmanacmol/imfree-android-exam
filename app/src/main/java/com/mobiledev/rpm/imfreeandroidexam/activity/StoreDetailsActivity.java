package com.mobiledev.rpm.imfreeandroidexam.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobiledev.rpm.imfreeandroidexam.Constant;
import com.mobiledev.rpm.imfreeandroidexam.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rmanacmol on 3/30/2017.
 */

public class StoreDetailsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.ivStoreImage) ImageView ivStoreImage;
    @Bind(R.id.tvStoreName) TextView tvStoreName;
    @Bind(R.id.tvStoreDesc) TextView tvStoreDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent i = getIntent();
        String storeName = i.getStringExtra(Constant.storeName);
        String storeDesc = i.getStringExtra(Constant.storeDesc);
        String storeImage = i.getStringExtra(Constant.storeImage);

        tvStoreName.setText(storeName);
        tvStoreDesc.setText(storeDesc);
        Glide.with(this)
                .load(storeImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivStoreImage);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.mobiledev.rpm.imfreeandroidexam.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rmanacmol on 3/30/2017.
 */

public class Store {

    @SerializedName("data")
    private List<StoreData> data;
    @SerializedName("meta")
    private String meta;
    @SerializedName("response_code")
    private int response_code;

    public List<StoreData> getData() {
        return data;
    }

    public void setData(List<StoreData> data) {
        this.data = data;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }
}
